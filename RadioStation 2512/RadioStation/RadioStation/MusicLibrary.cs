﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RadioStation
{//Не будет использоваться, т.к. можно заменить обычным списком List<Song>
    public class MusicLibrary<T> : IEnumerable<T> where T : Song, new()
    {
        public int Count { get { return songList.Count; } }
        private List<T> songList = new List<T>();
        public MusicLibrary(string fileName)
        {
            using (StreamReader sr = new StreamReader(fileName))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] s = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    var song = new T();
                    song.Album = s[0];
                    song.Artist = s[1];
                    song.Author = s[2];
                    song.Date = int.Parse(s[3]);
                    song.Duration = int.Parse(s[4]);
                    song.Genre = s[5];
                    song.Name = s[6];
                    song.Rating = int.Parse(s[7]);
                    songList.Add(song);
                }
            }
        }
        public void Add(T value)
        {
            songList.Add(value);

        }
        public void AddRange(IEnumerable<T> list)
        {
            if (list == null || !list.Any())
                throw new ArgumentException();
            foreach (var item in list)
            {
                Add(item);
            }
        }
        public bool Contains(string OName)
        {
            foreach (var item in songList)
            {
                if (item.Name == OName)
                {
                    return true;
                }
            }
            return false;
        }
        public void Sort()
        {
            var songArray = songList.ToArray();
            Array.Sort(songArray);
            songList = new List<T>(songArray);
        }
        public void Print()
        {

        }
        public Song this[int index]
        {
            get { return songList[index]; }
        }
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        { return songList.GetEnumerator(); }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        { return songList.GetEnumerator(); }
    }
}
