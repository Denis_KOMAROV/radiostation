﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioStation
{
    public abstract class RadioProgram
    {
        public string Genre { get; set; }
        public int Duration { get; set; }
        public abstract void Execute();

        public Song GetMostSuitableSong(List<Song> songsForChoosing, List<Song> playedSongs)
        {
            foreach (Song song in songsForChoosing)
            {
                song.Priority = 0;
            }

            foreach (Song songForChoosing in songsForChoosing)
            {
                foreach (Song playedSong in playedSongs)
                {
                    if (songForChoosing.Genre == playedSong.Genre)
                        songForChoosing.Priority += 10;
                    if (songForChoosing.Name == playedSong.Name)
                        songForChoosing.Priority += 1000;
                    if (songForChoosing.Artist == playedSong.Artist)
                        songForChoosing.Priority += 100;
                    if (songForChoosing.Author == playedSong.Author)
                        songForChoosing.Priority += 70;
                    if (songForChoosing.Album == playedSong.Album)
                        songForChoosing.Priority += 30;
                }
            }

            List<Song> songs = (from song in songsForChoosing orderby song.Priority ascending select song).ToList<Song>();
            return songs[0];
        }
    }

    public class RandomSongs : RadioProgram
    {
        public RandomSongs(int duration)
        {
            Duration = duration;
        }

        public override void Execute()
        {

        }

        public void Execute(List<Song> library, List<Song> playedSongs, ref int songFinishingTime)
        {
            Song chosenSong = new Song();
            Random random = new Random();
            chosenSong = GetMostSuitableSong(library, playedSongs);
            playedSongs.Add(chosenSong);
            songFinishingTime += chosenSong.Duration;
        }
    }
    public class HitParade: RadioProgram
    {
        private int songNumber;
        public HitParade(string genre, int duration)
        {
            Genre = genre;
            songNumber = 0;
            Duration = duration;
        }

        public override void Execute()
        {

        }

        public void Execute(List<Song> playedSongs, ref int songFinishingTime)
        {
            List<Song> songsToPlay =
                (from song in PlayWindow.startingOptions.MusicLibrary where song.Genre == this.Genre orderby song.Rating descending select song)
                    .ToList();
            playedSongs.Add(songsToPlay[this.songNumber]);
            songFinishingTime += songsToPlay[this.songNumber].Duration * 1000;
            this.songNumber++;
        }
    }

    public class SongRequestProgram : RadioProgram
    {
        public SongRequestProgram(int duration)
        {
            Duration = duration;
        }

        public override void Execute()
        {

        }

        public void Execute(List<Request> unexecuted, List<Request> executed, List<Song> playedSongs, ref int songFinishingTime)
        {
            Song chosenSong = new Song();
            Song s = new Song();
            int j = 0;
            List<int> a = new List<int>();
            chosenSong.Priority = 1000000;
            for (int i = 0; i < unexecuted.Count(); i++)
            {
                switch (unexecuted[i].Type)
                {
                    case 0://Artist                       
                            s =
                            GetMostSuitableSong(
                            (from song in PlayWindow.startingOptions.MusicLibrary
                             where song.Artist == unexecuted[i].Artist
                             select song).ToList(), playedSongs);
                            if (chosenSong.Priority > s.Priority)
                                chosenSong = s;            
                        break;
                    case 1://Author

                            s =
                            GetMostSuitableSong(
                            (from song in PlayWindow.startingOptions.MusicLibrary
                             where song.Author == unexecuted[i].Author
                             select song).ToList(), playedSongs);
                            if (chosenSong.Priority > s.Priority)
                                chosenSong = s;
                        break;
                    case 2://Name

                            s =
                            GetMostSuitableSong(
                            (from song in PlayWindow.startingOptions.MusicLibrary
                             where song.Name == unexecuted[i].Name
                             select song).ToList(), playedSongs);
                            if (chosenSong.Priority > s.Priority)
                                chosenSong = s;
                        break;
                    case 3://Album

                            s =
                            GetMostSuitableSong(
                            (from song in PlayWindow.startingOptions.MusicLibrary
                             where song.Album == unexecuted[i].Album
                             select song).ToList(), playedSongs);
                            if (chosenSong.Priority > s.Priority)
                                chosenSong = s;
                        break;
                }
            }
            foreach (Request r in unexecuted)
            {
                if (r.Album == chosenSong.Album || r.Artist == chosenSong.Artist || r.Author == chosenSong.Author || r.Name == chosenSong.Name)
                {
                    a.Add(j);
                    j++;
                }
            }
            foreach (int b in a)
            {
                executed.Add(unexecuted[b]);
                unexecuted.RemoveAt(b);
            }
            playedSongs.Add(chosenSong);
            songFinishingTime += chosenSong.Duration;
            //PlayWindow;
        }
    }

    public interface IRadioProcessor
    {
        void ExecuteProgram(RadioProgram program);
    }

    public class RadioProcessor: IRadioProcessor
    {
        public void ExecuteProgram(RadioProgram program)
        {

        }
    }

    public class Song
    {
        public string Name { get; set; }
        public string Genre { get; set; }
        public string Author { get; set; }
        public string Artist { get; set; }
        public string Album { get; set; }
        public int Date { get; set; }
        public int Rating { get; set; }
        public int Priority { get; set; }
        public int Duration { get; set; }

        public static bool operator == (Song first, Song second)
        {
            if (first.Name == second.Name && first.Genre == second.Genre && first.Author==second.Author && first.Artist== second.Artist && first.Album == second.Album && first.Duration==second.Duration)
                return true;
            else
                return false;
        }
        public static bool operator !=(Song first, Song second)
        {
            return !(first==second);
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}. Genre: {2}. Album: {3}.", Artist, Name, Genre, Album);
        }
    }


}
