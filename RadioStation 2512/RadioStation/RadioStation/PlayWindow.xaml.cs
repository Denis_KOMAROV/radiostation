﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RadioStation
{
    /// <summary>
    /// Логика взаимодействия для PlayWindow.xaml
    /// </summary>
    public partial class PlayWindow : Window
    {
        public static Options startingOptions;
        public static int numbOfPrograms;
        private RequestGenerator requestGenerator;
        private Random random;
        public static List<Request> unexecutedRequests;
        public static List<Request> executedRequests;
        private RadioProgram currentProgram;
        private List<string> genres = new List<string>() {"Rock", "Classic", "Jazz", "Pop", "Hip-Hop", "Electronic"};
        private System.Windows.Threading.DispatcherTimer mainTimer;
        private int programFinishingTime;
        private int songFinishingTime = 0;
        private int currentTime = 0;
        private int currentDay = 1;
        private int modelingFinishingTime;
        public static List<Song> playedSongs;
        //private List<Song> hitParadeSongs;
        


        public PlayWindow(Options options)
        {
            InitializeComponent();
            startingOptions = options;
            numbOfPrograms = startingOptions.NumberOfPrograms;
            requestGenerator = RequestGenerator.CreateInstance();
            random = new Random();
            unexecutedRequests = new List<Request>();
            executedRequests = new List<Request>();
            playedSongs = new List<Song>();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
           int numbOfPrograms = startingOptions.NumberOfPrograms;
           // notProgramTime = 24 - notProgramTime;
           // notProgramTime = notProgramTime / startingOptions.ProgramDuration;
           // if (notProgramTime % 2 == 1)
          //  {
           //     programFinishingTime = currentTime + notProgramTime / 2 *60*1000;
           // }
           // else
           // {
                currentProgram = new SongRequestProgram(startingOptions.ProgramDuration);
                startingOptions.NumberOfPrograms--;
                unexecutedRequests.AddRange(requestGenerator.GetRequests(random.Next(1, 3), genres[random.Next(genres.Count)], startingOptions.MusicLibrary));
                programFinishingTime = currentTime + currentProgram.Duration * 60 * 1000;
            //}
            unexecutedRequests.AddRange(requestGenerator.GetRequests(10, genres[random.Next(genres.Count)], startingOptions.MusicLibrary));
            programFinishingTime = currentTime + currentProgram.Duration * 60 * 1000;
            modelingFinishingTime = startingOptions.NumberOfDays * 1000 * 60 * 24;
            mainTimer = new System.Windows.Threading.DispatcherTimer();
            mainTimer.Tick += new EventHandler(timer_Tick);
            mainTimer.Interval = new TimeSpan(0,0,0,0,1000/startingOptions.ModellingStep);
            mainTimer.Start();
            btnStart.IsEnabled = false;
        }

        private void timer_Tick(object obj, EventArgs a)
        {
            RequestList.Clear();
            if (unexecutedRequests.Count >20)
                for (int i = 0; i < 20; i++)
                {
                    Request r = unexecutedRequests[i];
                    RequestList.Text += String.Format("{0}{1}{2}{3}", r.Name, r.Artist, r.Album, r.Author);
                    RequestList.Text += Environment.NewLine;
                }
            else
                for (int i = 0; i < unexecutedRequests.Count; i++)
                {
                    Request r = unexecutedRequests[i];
                    RequestList.Text += String.Format("{0}{1}{2}{3}", r.Name, r.Artist, r.Album, r.Author);
                    RequestList.Text += Environment.NewLine;
                }
            unexecutedRequests.AddRange(requestGenerator.GetRequests(random.Next(0,2), genres[random.Next(genres.Count)], startingOptions.MusicLibrary));
            currentTime += 1000;
            labelCurrentTime.Content = string.Format("{0} day {1}:{2}",currentDay,(currentTime/60000)%24,(currentTime/1000)%60);
            if (currentTime >= programFinishingTime)
            {
                PlayedProgramList.Text += string.Format("{0} {1} ",  currentProgram.Duration, currentProgram.Genre);
                if (currentProgram is SongRequestProgram)
                    PlayedProgramList.Text += "Song request program";
                else
                {
                    if (currentProgram is HitParade)
                    {
                        PlayedProgramList.Text += "Hit parade";
                    }
                    else
                    {
                        currentTime = 0;
                        currentDay++;
                        if (currentDay > startingOptions.NumberOfDays)
                        {
                            mainTimer.Stop();
                            EndOfModeling eom = new EndOfModeling();
                            eom.Show();
                            this.Close();
                        }
                        startingOptions.NumberOfPrograms = numbOfPrograms;
                        PlayedProgramList.Text += "Please Stand By";
                    }
                }
                PlayedProgramList.Text += Environment.NewLine;
                if (startingOptions.NumberOfPrograms != 0)
                {
                    GenerateNewProgram();
                    startingOptions.NumberOfPrograms--;
                }
                else
                {
                    currentProgram = new RandomSongs(24 - numbOfPrograms * startingOptions.ProgramDuration);
                }
                    programFinishingTime = currentTime + currentProgram.Duration * 60 * 1000;
            }
            if (currentTime >= songFinishingTime)
            {
                PlayedSongsList.Clear();
                if (playedSongs.Count>20)
                    for (int j = 1; j < 20; j++)
                    {
                        PlayedSongsList.Text += playedSongs[playedSongs.Count - j].ToString();
                        PlayedSongsList.Text += Environment.NewLine;
                    }
                else
                    for (int j = 1; j < playedSongs.Count; j++)
                    {
                        PlayedSongsList.Text += playedSongs[playedSongs.Count - j].ToString();
                        PlayedSongsList.Text += Environment.NewLine;
                    }
                if (currentProgram is SongRequestProgram)
                {
                    (currentProgram as SongRequestProgram).Execute(unexecutedRequests, executedRequests, playedSongs, ref songFinishingTime);
                    songFinishingTime = currentTime + playedSongs.Last().Duration * 1000;
                    lblType.Content = "Song Request";
                }
                else
                {
                    if (currentProgram is HitParade)
                    {
                        (currentProgram as HitParade).Execute(playedSongs, ref songFinishingTime);
                        songFinishingTime = currentTime + playedSongs.Last().Duration * 1000;
                        lblType.Content = "Hit-Parade";
                    }

                    else
                    {
                        (currentProgram as RandomSongs).Execute(startingOptions.MusicLibrary, playedSongs, ref songFinishingTime);
                        lblType.Content = "Please Stand By";
                        songFinishingTime = currentTime + playedSongs.Last().Duration * 1000;
                    }
                }
                lblArtist.Content = playedSongs.Last().Artist;
                lblSongName.Content = playedSongs.Last().Name;
                lblAuthor.Content = playedSongs.Last().Author;
                lblYear.Content = playedSongs.Last().Date;
                lblGenre.Content = playedSongs.Last().Genre;
                lblRating.Content = playedSongs.Last().Rating;
                lblSongTime.Content = String.Format("{0}:00", playedSongs.Last().Duration);
                switch (playedSongs.Last().Genre)
                {
                    case "Rock":
                        BitmapImage biRock = new BitmapImage();
                        biRock.BeginInit();
                        biRock.UriSource = new Uri("rock101x101.png", UriKind.Relative);
                        biRock.EndInit();
                        image.Stretch = Stretch.Fill;
                        image.Source = biRock;
                        break;
                    case "Classic":
                        BitmapImage biClassic = new BitmapImage();
                        biClassic.BeginInit();
                        biClassic.UriSource = new Uri("classical101x101.png", UriKind.Relative);
                        biClassic.EndInit();
                        image.Stretch = Stretch.Fill;
                        image.Source = biClassic; break;
                    case "Jazz":
                        BitmapImage biJazz = new BitmapImage();
                        biJazz.BeginInit();
                        biJazz.UriSource = new Uri("jazz101x101.png", UriKind.Relative);
                        biJazz.EndInit();
                        image.Stretch = Stretch.Fill;
                        image.Source = biJazz; break;
                    case "Pop":
                        BitmapImage biPop = new BitmapImage();
                        biPop.BeginInit();
                        biPop.UriSource = new Uri("pop101x101.png", UriKind.Relative);
                        biPop.EndInit();
                        image.Stretch = Stretch.Fill;
                        image.Source = biPop; break;
                    case "Hip-Hop":
                        BitmapImage biHipHop = new BitmapImage();
                        biHipHop.BeginInit();
                        biHipHop.UriSource = new Uri("hip-hop101x101.png", UriKind.Relative);
                        biHipHop.EndInit();
                        image.Stretch = Stretch.Fill;
                        image.Source = biHipHop; break;
                    case "Electronic":
                        BitmapImage biElectronic = new BitmapImage();
                        biElectronic.BeginInit();
                        biElectronic.UriSource = new Uri("electronic101x101.png", UriKind.Relative);
                        biElectronic.EndInit();
                        image.Stretch = Stretch.Fill;
                        image.Source = biElectronic;                        break;
                }
            }
        }

        private void GenerateNewProgram()
        {
            int R = random.Next(2);
            switch (R)
            {
                case 0:
                    currentProgram = new SongRequestProgram(startingOptions.ProgramDuration);
                    unexecutedRequests.AddRange(requestGenerator.GetRequests(startingOptions.ProgramDuration * 60 / 3, genres[random.Next(genres.Count)], startingOptions.MusicLibrary));
                    break;
                case 1:
                    currentProgram = new HitParade(genres[random.Next(genres.Count)], startingOptions.ProgramDuration);
                    break;
            }            
        }
        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            If (currentTime > 0)
            {
                mainTimer.Stop();
            }
            EndOfModeling eom = new EndOfModeling();
            eom.Show();
            this.Close();
        }
    }
}
