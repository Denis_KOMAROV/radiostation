﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioStation
{
    public class Request
    {
        public string Artist;
        public string Author;
        public string Name;
        public string Album;
        public int Type;

        public Request()
        {
            Artist = "";
            Author = "";
            Name = "";
            Album = "";
            Type = 0;
        }

        public Request(string artist = "", string author = "", string name = "", string album = "", int type = 0)
        {
            Artist = artist;
            Author = author;
            Name = name;
            Album = album;
            Type = type;
        }
    }

    public class RequestGenerator
    {
        private static RequestGenerator requestGeneratorInstance;

        protected RequestGenerator() { }
        public static RequestGenerator CreateInstance()
        {
            if (requestGeneratorInstance == null)
                requestGeneratorInstance = new RequestGenerator();
            return requestGeneratorInstance;
        }

        private static Random random = new Random();
        public List<Request> GetRequests(int numberOfRequests, string genre, List<Song> musicLibrary)
        {
            List<Request> requestList = new List<Request>();
            var musicLibraryOfNecessaryGenre =
                (from song in musicLibrary where song.Genre == genre select song).ToList<Song>();
            for (int i = 0; i < numberOfRequests; i++)
            {
                int parameterNumber = random.Next(0, 3);
                switch (parameterNumber)
                {
                    case 0: //Artist
                        var musicLibraryOfArtists =
                            (from song in musicLibraryOfNecessaryGenre select song.Artist).Distinct().ToList();
                        requestList.Add(
                            new Request(artist: musicLibraryOfArtists[random.Next(musicLibraryOfArtists.Count())], type: 0));
                        break;

                    case 1: //Author
                        var musicLibraryOfAuthors =
                            (from song in musicLibraryOfNecessaryGenre select song.Author).Distinct().ToList();
                        requestList.Add(
                            new Request(author: musicLibraryOfAuthors[random.Next(musicLibraryOfAuthors.Count())], type: 1));
                        break;
                    case 2: //Name
                        var musicLibraryOfNames =
                            (from song in musicLibraryOfNecessaryGenre select song.Name).Distinct().ToList();
                        requestList.Add(
                            new Request(name: musicLibraryOfNames[random.Next(musicLibraryOfNames.Count)], type: 2));
                        break;
                    case 3: //Album
                        var musicLibraryOfAlbums =
                            (from song in musicLibraryOfNecessaryGenre select song.Album).Distinct().ToList();
                        requestList.Add(
                            new Request(album: musicLibraryOfAlbums[random.Next(musicLibraryOfAlbums.Count)], type: 3));
                        break;

                }
            }
            foreach (Request request in requestList)
            {
                switch (request.Type)
                {
                    case 0://Artist
                        List<Song> songList =
                            (from song in PlayWindow.startingOptions.MusicLibrary
                             where song.Artist == request.Artist
                             select song).ToList();
                        foreach (Song song in songList)
                        {
                            song.Rating += 5;
                        }
                        break;
                    case 1://Author
                        songList =
                            (from song in PlayWindow.startingOptions.MusicLibrary
                             where song.Author == request.Author
                             select song).ToList();
                        foreach (Song song in songList)
                        {
                            song.Rating += 5;
                        }
                        break;
                    case 2://Name
                        songList =
                            (from song in PlayWindow.startingOptions.MusicLibrary
                             where song.Name == request.Name
                             select song).ToList();
                        foreach (Song song in songList)
                        {
                            song.Rating += 5;
                        }
                        break;
                    case 3://Album
                        songList =
                            (from song in PlayWindow.startingOptions.MusicLibrary
                             where song.Album == request.Album
                             select song).ToList();
                        foreach (Song song in songList)
                        {
                            song.Rating += 5;
                        }
                        break;
                }
            }
            return requestList;
        }
    }
}
