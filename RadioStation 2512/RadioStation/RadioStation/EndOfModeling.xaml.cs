﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RadioStation
{
    /// <summary>
    /// Логика взаимодействия для EndOfModeling.xaml
    /// </summary>
    public partial class EndOfModeling : Window
    {
        public EndOfModeling()
        {
            InitializeComponent();
            ShowResults();
        }
        public void ShowResults()
        {
            AddMessageTextResults("Starting modeling parameters:");
            AddMessageTextResults(String.Format("Days On-Air: {0}", PlayWindow.startingOptions.NumberOfDays));
            AddMessageTextResults(String.Format("Programs per one day: {0}", PlayWindow.numbOfPrograms));
            AddMessageTextResults(String.Format("Program duration: {0}", PlayWindow.startingOptions.ProgramDuration));
            AddMessageTextResults2("Played songs:");
            foreach (Song s in PlayWindow.playedSongs)
                AddMessageTextResults2(String.Format("{0}, {1}, {2}", s.Name, s.Artist, s.Genre));

            label.Content = "Total number of all requests:" + String.Format("{0}", PlayWindow.executedRequests.Count + PlayWindow.unexecutedRequests.Count);
        }
        private void AddMessageTextResults(string message)
        {
            txbResults.Text += message;
            txbResults.Text += Environment.NewLine;
        }
        private void AddMessageTextResults2(string message)
        {
            txbResults2.Text += message;
            txbResults2.Text += Environment.NewLine;
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}