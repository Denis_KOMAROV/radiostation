﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;

namespace RadioStation
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public Options options = new Options();
        public List<Song> mLibrary;

        private void buttonLoadLibrary_Click(object sender, EventArgs e)
        {
            LoadMusicLibrary();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadMusicLibrary()
        {
            OpenFileDialog of = new OpenFileDialog();
            if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    mLibrary = new List<Song>();
                    using (StreamReader sr = new StreamReader(of.FileName))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] s = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            var song = new Song();
                            song.Album = s[0];
                            song.Artist = s[1];
                            song.Author = s[2];
                            song.Date = int.Parse(s[3]);
                            song.Duration = int.Parse(s[4]);
                            song.Genre = s[5];
                            song.Name = s[6];
                            song.Rating = int.Parse(s[7]);
                            mLibrary.Add(song);
                        }
                    }
                    System.Windows.MessageBox.Show("Библиотека загружена!");
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
        }

        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            if (mLibrary == null)
            {
                System.Windows.MessageBox.Show("Библиотека не загружена!");
            }
            else 
            {
                try
                { if (int.Parse(textBox1.Text) <= 7 && int.Parse(textBox1.Text) >= 1 && int.Parse(textBox2.Text) >= 7 && int.Parse(textBox2.Text) <= 12 && int.Parse(textBox3.Text) >= 1 && int.Parse(textBox3.Text) <= 3 && int.Parse(textBox.Text)>=10 && int.Parse(textBox.Text)<=30)
                  {
                        if (int.Parse(textBox2.Text) > 8 && int.Parse(textBox3.Text) == 3)
                            System.Windows.MessageBox.Show("Эфир получается длиннее, чем часов в сутках! (Если длительность программы 3 часа, то может быть только до 8 программ в день)");
                        else
                        {
                            options = new Options(int.Parse(textBox1.Text), int.Parse(textBox2.Text), int.Parse(textBox3.Text), int.Parse(textBox.Text), mLibrary);
                            PlayWindow pw = new PlayWindow(options);
                            pw.Show();
                            this.Close();
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Проверьте параметры моделирования!");
                    }
                }
                catch
                {
                    System.Windows.MessageBox.Show("Проверьте параметры моделирования!");
                }
            }
        }
    }
}
