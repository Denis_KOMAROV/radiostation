﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioStation
{
    public class Options
    {
        public int NumberOfDays;//число дней
        public int NumberOfPrograms;//число радиопрограмм
        public int ProgramDuration;//длительность каждой радиопрограммы в часах
        public int ModellingStep;//шаг моделирования в минутах
        public List<Song> MusicLibrary;
        public Options() { }
        public Options(int numberOfDays, int numberOfPrograms, int programDuration, int modellingStep, List<Song> musicLibrary)
        {
            NumberOfDays = numberOfDays;
            NumberOfPrograms = numberOfPrograms;
            ProgramDuration = programDuration;
            ModellingStep = modellingStep;
            MusicLibrary = musicLibrary;
        }
    }
}
