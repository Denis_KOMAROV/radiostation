﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RadioStation
{
    /// <summary>
    /// Логика взаимодействия для PlayWindow.xaml
    /// </summary>
    public partial class PlayWindow : Window
    {
        public static Options startingOptions;
        private RequestGenerator requestGenerator;
        private Random random;
        public static List<Request> unexecutedRequests;
        public static List<Request> executedRequests;
        private RadioProgram currentProgram;
        private List<string> genres = new List<string>() {"Rock", "Classic", "Jazz", "Pop", "Hip-Hop", "Electronic"};
        private System.Windows.Threading.DispatcherTimer mainTimer;
        private int programFinishingTime;
        private int songFinishingTime = 0;
        private int currentTime = 0;
        public static List<Song> playedSongs;
        private List<Song> hitParadeSongs;
        //private Image image = new Image();


        public PlayWindow(Options options)
        {
            InitializeComponent();
            startingOptions = options;
            requestGenerator = RequestGenerator.CreateInstance();
            random = new Random();
            unexecutedRequests = new List<Request>();
            executedRequests = new List<Request>();
            playedSongs = new List<Song>();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            currentProgram = new SongRequestProgram(startingOptions.ProgramDuration);
            unexecutedRequests.AddRange(requestGenerator.GetRequests(startingOptions.ProgramDuration * 60 / 3, genres[random.Next(genres.Count)], startingOptions.MusicLibrary));
            programFinishingTime = currentTime + currentProgram.Duration * 60;
            mainTimer = new System.Windows.Threading.DispatcherTimer();
            mainTimer.Tick += new EventHandler(timer_Tick);
            mainTimer.Interval = new TimeSpan(0,0,1);
            mainTimer.Start();
        }

        private void timer_Tick(object obj, EventArgs a)
        {
            RequestList.Clear();
            foreach (Request r in unexecutedRequests)
            {
                RequestList.Text += String.Format("", r.Name, r.Artist, r.Album, r.Author);
                RequestList.Text += Environment.NewLine;
            }
            currentTime += 60;
            if (currentTime >= programFinishingTime)
            {
                PlayedProgramList.Text += string.Format("", currentProgram.Genre, currentProgram.Duration);
                if (currentProgram is SongRequestProgram)
                    PlayedProgramList.Text += "Song request program";
                else
                    PlayedProgramList.Text += "Hit parade";
                PlayedProgramList.Text += Environment.NewLine;
                GenerateNewProgram();
            }
            if (currentTime >= songFinishingTime)
            {
                if (playedSongs.Count > 0)
                {
                    PlayedSongsList.Text += playedSongs.Last().ToString();
                    PlayedSongsList.Text += Environment.NewLine;
                }
                if (currentProgram is SongRequestProgram)
                {
                    (currentProgram as SongRequestProgram).Execute(unexecutedRequests, executedRequests, playedSongs, ref songFinishingTime);
                }
                if (currentProgram is HitParade)
                {
                    (currentProgram as HitParade).Execute(playedSongs, ref songFinishingTime);
                }
                lblArtist.Content = playedSongs.Last().Artist;
                lblSongName.Content = playedSongs.Last().Name;
                lblAuthor.Content = playedSongs.Last().Author;
                lblYear.Content = playedSongs.Last().Date;
                lblGenre.Content = playedSongs.Last().Genre;
                lblRating.Content = playedSongs.Last().Rating;
                lblSongTime.Content = String.Format("", playedSongs.Last().Duration, ":00");
            }
        }

        private void GenerateNewProgram()
        {
            switch (random.Next(1))
            {
                case 0:
                    currentProgram = new SongRequestProgram(startingOptions.ProgramDuration);
                    unexecutedRequests.AddRange(requestGenerator.GetRequests(startingOptions.ProgramDuration * 60 / 3, genres[random.Next(genres.Count)], startingOptions.MusicLibrary));
                    break;
                case 1:
                    currentProgram = new HitParade(genres[random.Next(genres.Count)], startingOptions.ProgramDuration);
                    break;
            }
            programFinishingTime += currentProgram.Duration * 60;
            switch (currentProgram.Genre)
            {
                case "Rock":
                    image.Source = new BitmapImage(new Uri("rock101x101.png"));
                    break;
                case "Classic":
                    image.Source = new BitmapImage(new Uri("classical101x101.png"));
                    break;
                case "Jazz":
                    image.Source = new BitmapImage(new Uri("jazz101x101.png"));
                    break;
                case "Pop":
                    image.Source = new BitmapImage(new Uri("pop101x101.png"));
                    break;
                case "Hip-Hop":
                    image.Source = new BitmapImage(new Uri("hip-hop101x101.png"));
                    break;
                case "Electronic":
                    image.Source = new BitmapImage(new Uri("electronic101x101.png"));
                    break;
            }
        }
        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            EndOfModeling eom = new EndOfModeling();
            eom.Show();
        }
    }
}
